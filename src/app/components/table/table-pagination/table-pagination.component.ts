import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
} from '@angular/core';
export interface PaginatedResponse<T> {
  items: T[];
  total: number;
  page: number;
}
export interface PaginationValue {
  page: number;
  pageSize: number;
}
@Component({
  selector: 'app-table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrls: ['./table-pagination.component.scss'],
})
export class TablePaginationComponent {
  @Input() value: PaginationValue = { page: 1, pageSize: 5 };
  @Input() total = 0;
  @Input() pageSizes: number[] = [5, 10, 25, 50];
  @Output() valueChange = new EventEmitter<PaginationValue>();
  public totalPages: number = 0;
  public visiblePages: number[] = [];
  ngOnInit(): void {
    this.updateTotalPages();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['total'] || changes['value']) {
      console.log(this.value);
      this.updateTotalPages();

      this.updateVisiblePages();
    }
  }
  public selectPage(page: number): void {
    this.value = { ...this.value, page };
    console.log(this.value);
    this.updateVisiblePages();
    this.valueChange.emit(this.value);
  }

  private updateVisiblePages(): void {
    const length = Math.min(this.totalPages, this.value.pageSize);
    const startIndex = Math.max(
      Math.min(
        this.value.page - Math.ceil(length / 2),
        this.totalPages - length
      ),
      0
    );
    this.visiblePages = Array.from(
      new Array(length).keys(),
      (item) => item + startIndex + 1
    );
  }
  private updateTotalPages(): void {
    this.totalPages = Math.ceil(this.total / this.value.pageSize);
  }
  public selectPageSize(pageSize: string): void {
    this.value = { page: 1, pageSize: +pageSize };
    this.updateTotalPages();
    this.updateVisiblePages();
    this.valueChange.emit(this.value);
  }
}

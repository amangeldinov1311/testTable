import { Component, Input, OnInit } from '@angular/core';
import { TableData } from '../../data.model';
import { Subject, debounceTime } from 'rxjs';
import {
  PaginatedResponse,
  PaginationValue,
} from './table-pagination/table-pagination.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() dataSource: TableData[] = [];
  @Input() columns: { field: string; name: string }[] = [];
  sortColumn: string = '';
  sortDirection: 'asc' | 'desc' = 'asc';
  term: string = '';
  selectedColumns: string[] = [];
  filteredDataSource: TableData[] = [];
  pagination: PaginationValue = { page: 1, pageSize: 5 };
  isColumnListOpen: boolean = false;

  private searchSubject = new Subject<string>();
  public visibleItems: TableData[] = [];
  ngOnInit(): void {
    this.selectedColumns = this.columns.map((item) => item.field);
    this.filteredDataSource = this.dataSource;
    this.visibleItems = this.filteredDataSource.slice(
      0,
      this.pagination.pageSize
    );

    this.searchSubject.pipe(debounceTime(500)).subscribe((term) => {
      this.search(term);
    });
  }
  toggleColumnList(): void {
    this.isColumnListOpen = !this.isColumnListOpen;
  }

  closeColumnList(): void {
    debugger;
    this.isColumnListOpen = false;
  }
  toggleColumn(field: any): void {
    console.log(this.selectedColumns);
    console.log(field.target.value);
    const index = this.selectedColumns.indexOf(field.target.value);
    if (index !== -1) {
      this.selectedColumns.splice(index, 1);
    } else {
      this.selectedColumns.push(field.target.value);
    }
  }
  public onPageChange(paginationData: PaginationValue): void {
    this.pagination = paginationData;
    this.applyFiltersAndSort();
  }
  sortData(field: string): void {
    this.sortColumn = field;
    this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    this.applyFiltersAndSort();
  }
  search(filter: string): void {
    this.term = filter;
    this.pagination.page = 1;
    this.applyFiltersAndSort();
  }
  onSearchInputChange(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.searchSubject.next(filterValue);
  }
  private applyFiltersAndSort(): void {
    if (this.term.trim().length === 0) {
      this.filteredDataSource = [...this.dataSource];
    } else {
      this.filteredDataSource = this.dataSource.filter((item) => {
        return Object.keys(item).some((key) => {
          const value = item[key];
          if (typeof value === 'string') {
            return value.toLowerCase().includes(this.term.toLowerCase());
          } else if (Array.isArray(value)) {
            return value.some((v) =>
              v.toLowerCase().includes(this.term.toLowerCase())
            );
          }
          return false;
        });
      });
    }
    if (this.sortColumn) {
      this.visibleItems = this.filteredDataSource.sort((a, b) => {
        let valueA =
          this.sortColumn === 'name' && a['name']
            ? this.sortColumn === 'name'
              ? a['name'].first
              : a['name'].last
            : this.sortColumn === 'lastName' && a['name']
            ? a['name'].last
            : this.sortColumn === 'tags' && Array.isArray(a[this.sortColumn])
            ? a[this.sortColumn].join(', ')
            : a[this.sortColumn];

        let valueB =
          this.sortColumn === 'name' && b['name']
            ? this.sortColumn === 'name'
              ? b['name'].first
              : b['name'].last
            : this.sortColumn === 'lastName' && b['name']
            ? b['name'].last
            : this.sortColumn === 'tags' && Array.isArray(b[this.sortColumn])
            ? b[this.sortColumn].join(', ')
            : b[this.sortColumn];
        if (valueA === undefined) {
          valueA = '';
        }
        if (valueB === undefined) {
          valueB = '';
        }
        if (typeof valueA === 'string' && typeof valueB === 'string') {
          return this.sortDirection === 'asc'
            ? valueA.localeCompare(valueB)
            : valueB.localeCompare(valueA);
        } else {
          return this.sortDirection === 'asc'
            ? valueA - valueB
            : valueB - valueA;
        }
      });
    }

    const startIndex = (this.pagination.page - 1) * this.pagination.pageSize;

    const items = this.filteredDataSource.slice(
      startIndex,
      startIndex + this.pagination.pageSize
    );
    this.visibleItems = items;
    console.log(this.filteredDataSource);
  }
}

import { Component, OnInit } from '@angular/core';
import { data } from '../assets/data';
import { TableData } from './data.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  tableData: TableData[] = data;
  displayedColumns: { field: string; name: string }[] = [
    { field: '_id', name: 'id' },
    { field: 'isActive', name: 'Статус' },
    { field: 'balance', name: 'Баланс' },
    { field: 'picture', name: 'Картинка' },
    { field: 'age', name: 'Возраст' },
    { field: 'name', name: 'Имя' },
    { field: 'lastName', name: ' Фамилия' },
    { field: 'company', name: 'Компания' },
    { field: 'email', name: 'Почта' },
    { field: 'address', name: 'Адрес' },
    { field: 'tags', name: 'Теги' },
    { field: 'favoriteFruit', name: 'Любимый фрукт' },
  ];
  ngOnInit() {}
}

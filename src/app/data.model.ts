export class TableData {
  [key: string]: any; // Индексная сигнатура

  constructor(
    _id: string,
    isActive: boolean,
    balance: string,
    picture: string,
    age: number,

    company: string,
    email: string,
    address: string,
    tags: string[],
    favoriteFruit: string,
    name?: {
      first: string;
      last: string;
    }
  ) {}
}
